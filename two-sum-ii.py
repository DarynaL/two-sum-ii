class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        numbers = numbers[::-1]
        ind = 0
        l = len(numbers)
        prev=0
        for i in numbers:
            if(numbers[-1]>=0)&(i>target):
                ind+=1
                continue
            sub = numbers[ind+1::]
            ind2=ind+1
            for j in sub:
                if(i+j<target):
                    break
                elif(i+j==target):
                    return[l-ind2,l-ind]
                ind2+=1
            ind+=1
