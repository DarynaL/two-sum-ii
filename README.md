Two Sum II

We use greedy algorithm. We are walking through the reversed array. If array does not consist of any negative elements, we are removing all elements that are greater than target(but still counting indexes). 

After we finish, we are walking to the right from every element of reversed array, trying to find a pair value.When we found some, we return both indexes. 

Space complexity: O(logn)

Time complexity:O(nlogn)

Submission screenshot:

![screensub3](/uploads/16237e74e1191bcc38fad0405f512ac3/screensub3.png)
